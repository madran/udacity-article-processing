const express = require('express');
const router = express.Router();
const dotenv = require('dotenv');

dotenv.config();

const AylienTextAPI = require('aylien_textapi');
const textAPI = new AylienTextAPI({
    application_id: process.env.API_ID,
    application_key: process.env.API_KEY
});

router.post('/elsa', function (req, res) {

    textAPI.entityLevelSentiment(
        {
            url: req.body.url,
        },
        (error, response) => {
            if (error === null) {
                res.json(response);
            } else {
                res.json(error);
            }
        }
    );
});

router.post('/summarize', function (req, res) {

    textAPI.summarize(
        {
            url: req.body.url,
        },
        (error, response) => {
            if (error === null) {
                res.json(response);
            } else {
                res.json(error);
            }
        }
    )
});

module.exports = router;

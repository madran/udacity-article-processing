const port = 8000;

const express = require('express');

const server = express();
server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(express.static('dist'));

const indexRouter = require('./routes/routes');
server.use('/', indexRouter);

server.listen(
    port,
    () => {
        console.log('Server starting on port: ' + port);
    }
);

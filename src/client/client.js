import "normalize.css"
import "./sass/style.scss";
import "./sass/spinner.scss";
import { start } from "./js/main";

start();
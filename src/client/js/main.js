import { createSpinner, buildEntitiesTable } from './html'

function start() {

    function displayInputError(message) {

        document.querySelector('.form__error').innerHTML = message;

        const input = document.querySelector('.form__input');
        input.classList.add('form__input--error')
    }

    function hideInputError() {

        document.querySelector('.form__error').innerHTML = '';

        const input = document.querySelector('.form__input');
        input.classList.remove('form__input--error')
    }

    document.querySelector('.form__input').addEventListener('focus', hideInputError);

    document.querySelector('.form').addEventListener('submit', (e) => {

        e.preventDefault();

        const input = document.querySelector('.form__input');
        const url = input.value;

        if (url === '') {
            displayInputError("This field can't be empty.");
            return;
        }

        const texts = document.querySelectorAll('.article-data__text');
        for (const text of texts) {
            text.innerHTML = '';
        }

        const summarizationHeader = document.querySelector('.article-data--summarization');
        summarizationHeader.appendChild(createSpinner());

        fetch('/summarize', {
            method: 'POST',
            body: JSON.stringify({url: url}),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {

            return response.json();
        }).then((data) => {

            summarizationHeader.removeChild(summarizationHeader.lastChild);
            const text = document.querySelector('.article-data--summarization .article-data__text');
            text.innerHTML = data.sentences;
            text.classList.add('article-data__text--show');
        });

        const elsaHeader = document.querySelector('.article-data--elsa');
        elsaHeader.appendChild(createSpinner());

        fetch('/elsa', {
            method: 'POST',
            body: JSON.stringify({url: url}),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {

            return response.json();
        }).then((data) => {

            elsaHeader.removeChild(elsaHeader.lastChild);
            const text = document.querySelector('.article-data--elsa .article-data__text');
            text.appendChild(buildEntitiesTable(data.entities));
            text.classList.add('article-data__text--show');
        });
    });
}

export { start };
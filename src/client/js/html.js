function createSpinner() {
    const container = document.createElement('div');
    container.classList.add('spinner');

    const bar1 = document.createElement('div');
    bar1.classList.add('rect1');
    container.appendChild(bar1);

    const bar2 = document.createElement('div');
    bar2.classList.add('rect2');
    container.appendChild(bar2);

    const bar3 = document.createElement('div');
    bar3.classList.add('rect3');
    container.appendChild(bar3);

    const bar4 = document.createElement('div');
    bar4.classList.add('rect4');
    container.appendChild(bar4);

    const bar5 = document.createElement('div');
    bar5.classList.add('rect5');
    container.appendChild(bar5);

    return container;
}

function buildEntitiesTable(entities) {

    const table = document.createElement('table');
    table.classList.add('table');
    const row = document.createElement('tr');
    row.classList.add('table__row')
    const name = document.createElement('th');
    name.innerHTML = 'Entity name';
    name.classList.add('table__header')
    const number = document.createElement('th');
    number.innerHTML = 'Mention number';
    number.classList.add('table__header')
    const sentiment = document.createElement('th');
    sentiment.innerHTML = 'Sentiment';
    sentiment.classList.add('table__header')
    row.appendChild(name);
    row.appendChild(number);
    row.appendChild(sentiment);
    table.appendChild(row);


    for (const entity of entities) {
        const row = document.createElement('tr');
        row.classList.add('table__row')
        const name = document.createElement('td');
        name.innerHTML = entity.mentions[0].text
        name.classList.add('table__cell')
        const number = document.createElement('td');
        number.innerHTML = entity.mentions.length;
        number.classList.add('table__cell')
        const sentiment = document.createElement('td');
        sentiment.innerHTML = entity.overall_sentiment.polarity;
        sentiment.classList.add('table__cell')

        if (entity.overall_sentiment.polarity === 'positive') {
            name.classList.add('table__cell--positive')
            number.classList.add('table__cell--positive')
            sentiment.classList.add('table__cell--positive')
        }

        if (entity.overall_sentiment.polarity === 'negative') {
            name.classList.add('table__cell--negative')
            number.classList.add('table__cell--negative')
            sentiment.classList.add('table__cell--negative')
        }

        row.appendChild(name);
        row.appendChild(number);
        row.appendChild(sentiment);
        table.appendChild(row);
    }

    return table;
}

export { createSpinner, buildEntitiesTable };
const configMerge = require('webpack-merge');

const config = require('./webpack.server.config');

module.exports = configMerge(config, {
    mode: "production",
    target: 'node',
});
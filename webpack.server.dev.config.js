const NodemonPlugin = require('nodemon-webpack-plugin');
const configMerge = require('webpack-merge');

const config = require('./webpack.server.config');

module.exports = configMerge(config, {
    mode: "development",
    target: 'node',
    devServer: {
        port: 3000,
    },
    plugins: [
        new NodemonPlugin()
    ]
});
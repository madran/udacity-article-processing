const path = require("path");
const nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: "./src/server/server.js" ,
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "server"),
    },
    externals: [nodeExternals()],
}
const HtmlWebpackPlugin = require("html-webpack-plugin");
const configMerge = require('webpack-merge');

const config = require('./webpack.client.config');

module.exports = configMerge(config, {
    mode: "development",
    devServer: {
        proxy: {
            '/': {
                target: 'http://localhost:8000',
                secure: false
            },
            '/elsa': {
                target: 'http://localhost:8000',
                secure: false
            },
            '/summarize': {
                target: 'http://localhost:8000',
                secure: false
            },
            '/sentiment': {
                target: 'http://localhost:8000',
                secure: false
            }
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/client/views/layout.html"
        })
    ],
    module: {
        rules: [
            {
                test: /.(sc|c)ss$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    }
});